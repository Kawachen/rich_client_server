package com.example.rich_client_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
public class RichClientServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RichClientServerApplication.class, args);
    }
}
